import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      alias: "/info",
      name: "info",
      component: () => import("../scenes/InfoDomain")
    },
    {
      path: "/domains/:host",
      name: "domain-details",
      component: () => import("../scenes/Domain")
    }
  ]
});