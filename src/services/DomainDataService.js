import http from "../helpers/http";

class DomainDataService {
  getAll() {
    return http.get("/domains");
  }

  analyze(host) {
    return http.get(`/domains/analyze?host=${host}`);
  }
}

export default new DomainDataService();