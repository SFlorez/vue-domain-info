const config = {
  apiUrl: process.env.VUE_APP_API_URL || "http://localhost:3000/api"
};

export default config;